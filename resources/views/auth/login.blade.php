@extends('layouts.front.apphome')

@section('content')
<div class="main">



	<div class="login">
		<div class="row">
				<div class="col-md-12">
			</div>
			<div class="col-md-4">
			</div>
			<div class="col-md-4" style="padding-left: 100px; padding-right: 100px;">
							<h4>Patient Login</h4>
				 <div class="col-md-12">@include('layouts.errors-and-messages')</div>
				 <form action="{{ route('login') }}" method="post" >
					{{ csrf_field() }}
					<div class="form-group clearfix"><i class="fa fa-envelope-o"></i>
					<!--<input type="text" class="form-control" placeholder="Email Address">-->
					<input type="email" class="form-control" placeholder="email" required id="email" name="email" value="{{ old('email') }}" style="border-radius:5px 5px 0 0">

					</div>
					<div class="form-group clearfix"><i class="fa fa-key"></i>
					<!--<input type="password" class="form-control" placeholder="Password">-->
					<input type="password" name="password" id="password" value="" class="form-control" placeholder="Password" required style="border-radius:0 0 5px 5px">

					</div>
				<!-- 	<div class="form-group clearfix"><a href="{{route('password.request')}}">Forgot Your Password?</a></div> -->
					<div class="form-group clearfix"><button class="btn-log">Login</button></div>
					<!-- <p>Don't Have an Account? <a href="{{ route('register') }}"> Sign up </a></p> -->
				</form>
			</div>
			<div class="col-md-4">
			</div>
		</div>
	</div>

</div>
@endsection