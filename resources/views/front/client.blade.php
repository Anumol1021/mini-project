@extends('layouts.front.apphome')
@section('content')
@include('layouts.front.home-slider')


<div class="container space-sm">
    <div class="col-md-12">
        <div class="book-box row">
            <div class="book-form">
                <h4 class="hr-after" style="text-align: center;">Details</h4>
                <div class="row" style=" padding-left: 20px; padding-right: 25px;text-align: center;">
				{{auth()->user()->name}}
                </div>
				<div class="row"style=" padding-left: 20px; padding-right: 25px;text-align: center;">
				{{auth()->user()->email}}
                </div>
				<div class="row" style=" padding-left: 20px; padding-right: 25px;text-align: center;">
				{{auth()->user()->phone}}
                </div>
				<div class="row" style=" padding-left: 20px; padding-right: 25px;text-align: center;">
				{{auth()->user()->address}}
                </div>
				<div class="row" style=" padding-left: 20px; padding-right: 25px;text-align: center;">
				{{auth()->user()->dob}}
                </div>
				<div class="row" style=" padding-left: 20px; padding-right: 25px;text-align: center;">
				{{auth()->user()->gendor}}
                </div>
				<div class="row" style=" padding-left: 20px; padding-right: 25px;text-align: center;">
				{{auth()->user()->place}}
                </div>
				<div class="row"style=" padding-left: 20px; padding-right: 25px;text-align: center;">
				{{auth()->user()->datee}}
                </div>
				<div class="row" style=" padding-left: 20px; padding-right: 25px;text-align: center;">
				{{auth()->user()->timee}}
                </div>
			</div>
        </div>
    </div>
</div>
@endsection