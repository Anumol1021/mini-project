<!DOCTYPE html>
<html>
<head></head>
<body style='background: white; color: #222'>
<table width='750px' border='0'>
    <thead>
    <tr><th colspan='2' style='color:#FFF'><img style='margin-left: 350px;height:75px;' src='{!! asset('resources/assets/front/') !!}/images/email_logo.png'>
	<h4 style="color: black">Purchase Order From Merwad <span style="float:right">Order Date : {{ date('M d, Y h:i a', strtotime($order->created_at)) }}</span></h4>
	</th>
	
	<th>
		<h4 style="color: black"></h4>
	</th>
	</tr>
	<tr>
		<th>
			OrderID #{{$order->reference}}
		</th>
	</tr>
    </thead>
    <tbody>
    <tr><td>Name</td><td>{{$customer->name}}</td></tr>
    <tr><td>Email</td><td>{{$customer->email}}</td></tr>
    <tr><td>Phone</td><td>{{$address->phone}}</td></tr>
    <tr><td style="vertical-align: top">Billing Address</td><td>
	
				 {{ $address->address_1 }} , {{ $address->address_2 }} <br />
            {{ $address->city }} <br />
            {{ $address->country }} {{ $address->zip }}</td></tr>
    <tr><td style="vertical-align: top">Shipping Address</td><td>
				{{ $shipping_address->address_1 }} , {{ $shipping_address->address_2 }} <br />
				{{ $shipping_address->city }}  <br />
				{{ $shipping_address->country }} {{ $shipping_address->zip }}</td></tr>
    <tr><td colspan='2'>
            <table width='750px' class='table' border='0'>
                <thead style='background:#949494;'>
                <tr>
                    <th colspan="2">Item</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>

                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
               @foreach($products as $product)
                    <tr style='text-align:center; border-bottom: #222 solid; '>




                        <td><img style='width:50px;' src='{{ asset("storage/products/thumb270x300_$product->cover") }}'></td> 
                        <td style='text-align:left;'>{{$product->name}}
						<p style="font-size: 10px;">
							@if(json_decode($product->pivot->productAttribute))
								@foreach(json_decode($product->pivot->productAttribute) as $key=>$option)
									<h5>{{ $key }} :  {{ $option }}</h5>
								@endforeach
							@endif
							</p>	
						</td>
                        <td>QAR {{$product->price}}</td>
                        <td>{{$product->pivot->quantity}}</td>

                        <td>QAR {{number_format($product->price * $product->pivot->quantity, 2)}}</td>
						
                    </tr>
                @endforeach
				<tr style='background:#949494;color:#FFF;text-align:right;'>
                    <td  colspan='3' style=' padding:0.8em;'>Subtotal</td>
                    <td  colspan='3'  style=' padding:0.8em;'>{{$order->total_products}}</td>
                </tr>
                <tr style='background:#949494;color:#FFF;text-align:right;'>
                    <td  colspan='3' style=' padding:0.8em;'>{{$order->courier}}</td>
                    <td  colspan='3'  style=' padding:0.8em;'>{{$order->shipping_charge}}</td>
                </tr>
				
				@if((float)$order->discounts > 0)
				<tr style='background:#949494;color:#FFF;text-align:right;'>
				
					<td  colspan='3' style=' padding:0.8em;'>Discount ( {{$order->coupon}})</td>
                    <td  colspan='3'  style=' padding:0.8em;'>{{$order->discounts}}</td>
					
				</tr>
				@endif
                <tr style='background:#949494;color:#FFF;text-align:right;'>
                    <td  colspan='3' style=' padding:0.8em;'>TOTAL</td>
                    <td  colspan='3'  style=' padding:0.8em;'>{{$order->total}}</td>
                </tr>
                </tbody>
            </table>
        </td></tr>
    </tbody>
</table>
</body>
</html>
