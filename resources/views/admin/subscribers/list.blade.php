@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$subscribers->isEmpty())
            <div class="box">
                <div class="box-body">
                    <h2>Subscribers</h2>
                    @include('layouts.search', ['route' => route('admin.subscribers')])
                    @include('admin.subscribers.email')
                    {{ $subscribers->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection
