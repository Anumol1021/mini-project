<?php

namespace App\Shop\Subscribers\Exceptions;

class SubscriberNotFoundException extends \Exception
{
}
