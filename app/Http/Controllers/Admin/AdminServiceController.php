<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 2.1
*/
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;

use App;
use Lang;

use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;
use App\Libraries\Slug;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;


class AdminServiceController extends Controller
{
	private $slug;

    public function __construct(Slug $slug)
    {
		$this->slug = $slug;
    }

    public function listingAppointment(Request $request){
		$title = array('pageTitle' => Lang::get("labels.listingAppointment"));		
		
		$result = array();
		$message = array();
			
		$appointments = DB::table('appointments')->paginate(20);
		
		$result['message'] = $message;
		$result['appointments'] = $appointments;
		
		return view("admin.listingAppointment", $title)->with('result', $result);
	}


	//addTaxClass
	public function addAppointment(Request $request){
		$title = array('pageTitle' => Lang::get("labels.Service"));
		$result = array();
		$message = array();
		$result['message'] = $message;
		return view("admin.addAppointment", $title)->with('result', $result);
	}
	public function addNewAppointment(Request $request){
		$title = array('pageTitle' => Lang::get("labels.Service"));
		if($request->hasFile('newImage')){
			$image = $request->newImage;
			$fileName = time().'.'.$image->getClientOriginalName();
			$image->move('resources/assets/images/banner_images/', $fileName);
			$uploadImage = 'resources/assets/images/banner_images/'.$fileName; 
		}else{
			$uploadImage = '';
		}
		if($request->hasFile('shortimage')){
			$image = $request->shortimage;
			$fileName = time().'.'.$image->getClientOriginalName();
			$image->move('resources/assets/images/banner_images/', $fileName);
			$uploadSmallImage = 'resources/assets/images/banner_images/'.$fileName; 
		}else{
			$uploadSmallImage = '';
		}
		$id = DB::table('appointments')->insertGetId([
			'name'  	   =>  $request->name,
			'slug'		   =>$this->slug->createSlug(strip_tags($request->name),'appointments'),
			'shortdes'     =>  $request->shortdes,
			'description'  =>  $request->description,
			'image'		   =>  $uploadImage,
			'shortimage'   =>  $uploadSmallImage,
			'status'	   =>  $request->status
		]);
		$message = "Details has been added successfully!";
		return redirect()->back()->withErrors([$message]);
	}
	
	public function editAppointment(Request $request){		
		$title = array('pageTitle' => Lang::get("labels.EditDetails"));
		$result = array();		
		$result['message'] = array();
		$appointments = DB::table('appointments')->where('id', $request->id)->get();
		$result['appointments'] = $appointments;
		return view("admin.editAppointment",$title)->with('result', $result);
	}
	public function updateAppointment(Request $request){
		$title = array('pageTitle' => Lang::get("labels.EditDetails"));
		
		
		$message = "Status has been updated successfully!";
		$companyUpdate = DB::table('appointments')->where('id', $request->id)->update([
			'status'	   =>   $request->status
		]);
		return redirect()->back()->withErrors([$message ]);
	}
	public function deleteAppointment(Request $request){
		DB::table('appointments')->where('id', $request->id)->delete();
		return redirect()->back()->withErrors("Details Deleted");
	}

	
}                                                                                     
