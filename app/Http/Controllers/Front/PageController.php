<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Subscribers\Subscribers;
//validator is builtin class in laravel
use Validator;
use App;
use Lang;
use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 

class PageController extends Controller
{
	public function index(string $slug){
		$view = 'front.page';
		$page = DB::table('pages_description')
			->select('pages_description.type','pages_description.description','pages_description.name' ,'pages_description.id')
			->where('pages_description.slug','=', $slug)
			->where('pages_description.status','=', 1)
			->get();
		
		if($page->count() <= 0){
			abort(404);exit;
		}	
		return view($view,compact('page'));
	}

	public function tribute(string $slug){
		$view = 'front.tribute_details';
		
		$pages = DB::table('pages_description')
			->where('slug','=', $slug)
			->where('status','=', 1)
			->get();
		$tribute = DB::table('pages_description')->where('status', '=', 1)->where('slug','<>',$slug)->get();	
		if($pages->count() <=0){
			abort(404);exit;
		}			
		return view($view, ['page'=>$pages,'tribute'=>$tribute]);

	}

	public function about(string $slug){
		$view = 'front.about_details';
		
		$pages = DB::table('pages_description')
			->where('slug','=', $slug)
			->where('status','=', 1)
			->get();
		$about = DB::table('pages_description')->where('status', '=', 1)->where('slug','<>',$slug)->get();	
		if($pages->count() <=0){
			abort(404);exit;
		}			
		return view($view, ['page'=>$pages,'about'=>$about]);

	}
	
}
