<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use DB;
use App\Shop\Customers\Requests\RegisterCustomerRequest;
use Illuminate\Http\Request;

class HomeController extends Controller
{
  
    public function index()
    {
        $view = 'front.index';
        return view($view);
    }

    public function login()
    {
        $view = 'front.login';
        return view($view);
    }
	
	public function loginn()
    {
        $view = 'front.login';
        return view($view);
    }
	
	public function client()
	{
		
		$view = 'front.client';
		return view($view);
	}
	
	public function registration()
    {
		$timee = \DB::table('time')->get();
        $view = 'front.registration';
        return view($view,compact('timee'));
    }
	
	public function register(RegisterCustomerRequest $request)
	{


		$id = DB::table('customers')->insertGetId([
            'name'   => $request->name,
			'address' => $request->address,
			'place' => $request->place,
			'gender' => $request->gender,
			'dob' => $request->dob,
			'phone' => $request->phone,
			'datee'    => $request->datee,
			'datee'    => $request->datee,
			'password' => bcrypt($request->password),
			//bcrypt($params['password']);
			'timee' => $request->timee,
			//'loginid' => 
		]);
		
		DB::table('appointments')->insert([
		'name'     => $request->name,
		'regid'    => $id,
		'tokenno'  => $id,
		'datee'    => $request->datee,
		'address'  => $request->address,
		'place'    => $request->place,
		'gender'   => $request->gender,
		'dob'      => $request->dob,
		'phone'    => $request->phone,
		'timee'    => $request->timee,
		'status'   => 'Active',
		]);
		
		
		$message = "Appointment has been submitted successfully!";
        return redirect()->back()->withErrors([$message]);
	}
	
	
	
	
	 
	
	

    // public function solution()
    // {
    //     $solution = DB::table('solution')->where('status', '=', 'Active')->get();
    //     $view = 'front.solution';
    //     return view($view,compact('solution'));
    // }

    // public function services(Request $request){
    //     $service = DB::table('service')->where('status', '=', 'Active')->get();
    //     $view = 'front.services';
    //     return view($view, compact('service'));    
    // }

    // public function servicesdetails(string $slug)
    // {
    //     $view = 'front.servicesdetails';
    //     $brands = DB::table('brands')->where('status', '=', 'Active')->where('servicename', 'like', $slug)->get();
    //     $service = DB::table('service')->where('status', '=', 'Active')->get();
    //     $servicesdetails = DB::table('service')
    //     ->where('slug', '=', $slug)
    //     ->where('status', '=', 'Active')
    //     ->first();
    //     if(!$servicesdetails){
    //         abort(404);exit;
    //     }   
    //     return view($view,compact('servicesdetails','service','brands'));
    // }


}
