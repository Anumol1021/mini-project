<?php

namespace App\Http\Middleware;

use Closure;
use DB;
class Common
{
	/**
     * @var ProductRepositoryInterface
     */
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next )
    {
		$tbl_clearance 		= "clearance";
		$view = 'front.index';
		if(app()->getLocale()=='ar'){
			$tbl_clearance 		= "ar_clearance";
		}
		$clearance = DB::table($tbl_clearance)->where('status', '=', 1)->paginate(5);
		view()->share('clearance_footer', $clearance);
		
        //return redirect(route('admin.login'));
        return $next($request);
    }
}
