<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Al Rabban</title>
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/css/settings.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/css/webslidemenu.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/css/aos.css">
		<link rel="stylesheet" type="text/css" href="<?php echo asset('resources/assets/front'); ?>/css/stylesheet.css">
		<!--<link rel="icon" type="image/png" sizes="192x192" href="<?php echo asset('resources/views/front'); ?>/images/favicon.ico">-->
		<script src="<?php echo asset('resources/assets/front'); ?>/js/jquery-2.1.0.min.js" type="text/javascript"></script>
		<script src="<?php echo asset('resources/assets/front'); ?>/js/bootstrap.min.js" type="text/javascript"></script>
	</head>
	<body>
		<?php echo $__env->make('layouts.front.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		<?php echo $__env->yieldContent('content'); ?>

		<?php echo $__env->make('layouts.front.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/webslidemenu.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/jquery.themepunch.revolution.min.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/owl.carousel.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/jquery.easeScroll.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/aos.js"></script> 
		<script type="text/javascript" src="<?php echo asset('resources/assets/front'); ?>/js/main.js"></script>
	</body>
</html>
