<?php $__env->startSection('content'); ?>
<!-- Main content -->
    <section class="content">
    <?php echo $__env->make('layouts.errors-and-messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Subscribers</h2>
            <div class="row">
              <div class="col-xs-12">
           
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Email Address</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php if(count($result['subscribers'])>0): ?>
                    <?php $__currentLoopData = $result['subscribers']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$subscribers): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($subscribers->id); ?></td>
                            <td><?php echo e($subscribers->email_address); ?></td>
                             <td><?php echo e($subscribers->created_date); ?></td>
                          
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  <?php echo e($result['subscribers']->links()); ?>

                </div>
              </div>
            </div>
          </div>
     </section>  

    <!-- deleteBannerModal -->
  <div class="modal fade" id="deleteSubscribersModal" tabindex="-1" role="dialog" aria-labelledby="deleteSubscribersModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="deleteSubscribersModalLabel">Delete Subscribers</h4>
      </div>
      <?php echo Form::open(array('url' =>'admin/deleteSubscribers', 'name'=>'deleteSubscribers', 'id'=>'deleteSubscribers', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

          <?php echo Form::hidden('action',  'delete', array('class'=>'form-control')); ?>

          <?php echo Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'id')); ?>

      <div class="modal-body">            
        <p>Are you sure you want to delete this Subscribers?</p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary" id="deleteBanner">Delete</button>
      </div>
      <?php echo Form::close(); ?>

    </div>
    </div>
  </div>
       
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('js'); ?>
<script>
$(document).on('click', '#deleteSubscribersId', function(){
var id = $(this).attr('Subscribersid');
$('#id').val(id);
$("#deleteSubscribersModal").modal('show');
});
</script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>