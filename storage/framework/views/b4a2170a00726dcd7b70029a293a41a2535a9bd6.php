<?php $__env->startSection('content'); ?>
<!-- Main content -->
    <section class="content">
    <?php echo $__env->make('layouts.errors-and-messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Descriptions</h2>
            <div class="row">
              <div class="col-xs-12">
         <div class="btn-group pull-right" >
                        <a class="btn btn-sm btn-primary" href="<?php echo e(route('admin.addDescription')); ?>"><i class="fa fa-plus"></i> Add New</a>
                 </div>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Register Id</th>
                      <th>Description</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php if(count($result['description'])>0): ?>
                    <?php $__currentLoopData = $result['description']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$description): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($description->id); ?></td>
                            <td><?php echo e($description->regid); ?></td>
                            <td><?php echo substr($description->description,0,200 ); ?></td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editDescription/<?php echo e($description->id); ?>" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                            
                           
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  <?php echo e($result['description']->links()); ?>

                </div>
              </div>
            </div>
          </div>
     </section>  



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>