<?php $__env->startSection('content'); ?>
<section class="content">
    <div class="box">
        <div class="box-body">
            <h2>Edit Details</h2>
           	<div class="box-body">
            <div class="row">
                <div class="col-xs-12">
              		<div class="box box-info">
                       <br>                       
                        <?php if(count($errors) > 0): ?>
                              <?php if($errors->any()): ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <?php echo e($errors->first()); ?>

                                </div>
                              <?php endif; ?>
                          <?php endif; ?>
                       
						<div class="panel with-nav-tabs  panel-default">
							
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="English">
										<div class="box-body">
											<?php echo Form::open(array('url' =>'admin/updateDescription', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

											  
						                    <?php echo Form::hidden('id',  $result['description'][0]->id , array('class'=>'form-control', 'id'=>'id')); ?>


												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Register Id</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('regid', $result['description'][0]->regid, array('class'=>'form-control','id'=>'regid','readonly')); ?>

												  </div>
												</div>

												<div class="form-group">
					                                <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Description')); ?>  </label>
					                                <div class="col-sm-10 col-md-8">
					                                  <textarea id="editor" name="description" class="form-control" rows="10" cols="80"><?php echo $result['description'][0]->description; ?></textarea> <br>
					                                </div>
				                             	</div>

											    <div class="box-footer text-center">
													<button type="submit" class="btn btn-primary"><?php echo e(trans('labels.Update')); ?></button>
													<a href="<?php echo e(URL::to('admin/listingDescription')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
											    </div>
											<?php echo Form::close(); ?>

										</div>
									</div>
                  				</div>
              				</div>
            			</div>
         			</div>
        		</div>
   			</div>
</section>

<script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>

<script type="text/javascript">
		$(function () {
			
			$("textarea").summernote({height: "400px",});
			
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>