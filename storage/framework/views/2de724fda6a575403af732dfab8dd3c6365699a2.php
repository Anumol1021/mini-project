<?php $__env->startSection('content'); ?>

<section class="abt-tp">
	<div class="container">
		<div class="col-md-12">
			<h3>Contact Us</h3>
			<ul class="breadcrumb">
			  <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
			  <li class="breadcrumb-item active">Contact Us</li>
			</ul>
		</div>
	</div>
</section>

<section class="con-sec">
	<div class="container">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6">
					<div class="con-lft">
						<ul>
							<li> <img src="<?php echo asset('resources/assets/front'); ?>/images/loc.svg"> Al Rabban Holding Co, P.O.Box 885, Qatar.</li>
							<li> <img src="<?php echo asset('resources/assets/front'); ?>/images/phn.svg"> <a href="tel:+974 4487 7662">+974 4487 7662</a></li>
							<li> <img src="<?php echo asset('resources/assets/front'); ?>/images/fax.svg"> +974 4487 5577</li>
							<li> <img src="<?php echo asset('resources/assets/front'); ?>/images/mail.svg"> <a href="mailto:info@alrabbanholding.com">info@alrabbanholding.com</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-6">
				 <?php if($errors->all()): ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="box no-border">
                            <div class="box-tools">
                                <p class="alert alert-warning alert-dismissible">
                                    <?php echo e($message); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 <?php elseif(session()->has('message')): ?>
                    <div class="box no-border">
                        <div class="box-tools">
                            <p class="alert alert-success alert-dismissible">
                                <?php echo e(session()->get('message')); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </p>
                        </div>
                    </div>
                 <?php elseif(session()->has('error')): ?>
                    <div class="box no-border">
                        <div class="box-tools">
                            <p class="alert alert-danger alert-dismissible">
                                <?php echo e(session()->get('error')); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </p>
                        </div>
                    </div>
                 <?php endif; ?>
					<form action="<?php echo e(route('contactus.send')); ?>" id="contactForm" method="post">
						<?php echo e(csrf_field()); ?>

						<div class="form-group"><input type="text" class="form-control" name="name" id="name"placeholder="Enter Your Name" value="" required><div class="form_line"></div></div>

						<div class="form-group"><input type="email" class="form-control" name="email" id="email" required placeholder="Enter Your Email Address"><div class="form_line"></div></div>

						<div class="form-group"><input type="text" class="form-control" name="phone" id="phone" placeholder="Enter Your Phone Number"><div class="form_line"></div></div>

						<div class="form-group"><textarea class="form-control" name="comment" id="comment" placeholder="Your Message" required></textarea><div class="form_line"></div></div>
						<div class="form-group"><button class="con-btn">SUBMIT</button></div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7212.7670239710105!2d51.52112802134933!3d25.324908778107506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e45dc78abe862b5%3A0xf84c1ddb201ec448!2sAl+Rabban+Holding!5e0!3m2!1sen!2sqa!4v1543999198402" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>