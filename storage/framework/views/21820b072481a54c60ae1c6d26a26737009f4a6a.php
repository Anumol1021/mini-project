<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 0.1.0
    </div>
    <strong>Copyright &copy; <?php echo e(date('Y')); ?> - <?php echo e(date('Y') + 1); ?> <a href="<?php echo e(config('app.url')); ?>"><?php echo e(config('app.name')); ?></a>.</strong> All rights
    reserved.
</footer>