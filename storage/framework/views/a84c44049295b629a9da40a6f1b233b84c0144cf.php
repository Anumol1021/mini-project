<?php $__env->startSection('content'); ?>
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Edit Page</h2>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
                  <div class="box box-info">
                    <br>                       
                        <?php if(count($errors) > 0): ?>
                              <?php if($errors->any()): ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <?php echo e($errors->first()); ?>

                                </div>
                              <?php endif; ?>
                          <?php endif; ?>
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->  
            <div class="panel with-nav-tabs  panel-default">
              <!--<div class="panel-heading" style="border:none;">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#English" data-toggle="tab">English</a></li>
                    <li><a href="#Arabic" data-toggle="tab">Arabic</a></li>
                  </ul>
              </div>-->
              <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="English">
            
                     <div class="box-body">
                     
                      <?php echo Form::open(array('url' =>'admin/updatePage', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

                        
            <?php echo Form::hidden('id',  $result['pages_description'][0]->id , array('class'=>'form-control', 'id'=>'id')); ?>


                            
                      <div class="form-group slug">
                          <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.PageSlug')); ?> </label>
                          <div class="col-sm-10 col-md-4">
                          <?php echo Form::text('slug', $result['pages_description'][0]->slug, array('class'=>'form-control','id'=>'slug')); ?>

                          </div>
                        </div>


                        <div class="form-group">
                          <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Title')); ?> </label>
                          <div class="col-sm-10 col-md-4">
                          <?php echo Form::text('name', $result['pages_description'][0]->name, array('class'=>'form-control','id'=>'name')); ?>

                          </div>
                        </div>




                         <div class="form-group">
                          <label for="name" class="col-sm-2 col-md-3 control-label">Type</label>
                          <div class="col-sm-10 col-md-4">
                            <select class="form-control" name="type">
                              <option value="1" <?php if($result['pages_description'][0]->type==1): ?> selected <?php endif; ?>><?php echo e(trans('labels.Default')); ?></option>
                              <option value="0" <?php if($result['pages_description'][0]->type==0): ?> selected <?php endif; ?>><?php echo e(trans('labels.Tribute')); ?></option>
                            </select>
                          </div>
                        </div>
                      

                        <div class="form-group">
                                          <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Description')); ?>  </label>
                                          <div class="col-sm-10 col-md-8">
                                            <textarea id="editor" name="description" class="form-control" rows="10" cols="80"><?php echo $result['pages_description'][0]->description; ?></textarea> <br>
                                          </div>
                                      </div>
                        
                        
                        <div class="form-group">
                          <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Status')); ?></label>
                          <div class="col-sm-10 col-md-4">
                            <select class="form-control" name="status">
                              <option value="1" <?php if($result['pages_description'][0]->status==1): ?> selected <?php endif; ?>><?php echo e(trans('labels.Active')); ?></option>
                              <option value="0" <?php if($result['pages_description'][0]->status==0): ?> selected <?php endif; ?>><?php echo e(trans('labels.Inactive')); ?></option>
                            </select>
                          </div>
                        </div>
                        
                        
                        <!-- /.box-body -->
                        <div class="box-footer text-center">
                        <button type="submit" class="btn btn-primary"><?php echo e(trans('labels.Update')); ?></button>
                        <a href="<?php echo e(URL::to('admin/listingPages')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
                        </div>
                        <!-- /.box-footer -->
                      <?php echo Form::close(); ?>

                    </div>
                  </div>
              
            
                  </div>
              </div>
            </div>
            
          </div>
         <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
   
  </section>
  <!-- /.content --> 
  <script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>


<script type="text/javascript">
    $(function () {
      
      //bootstrap WYSIHTML5 - text editor
      //
      $("textarea").summernote({height: "400px",});
      //$("textarea").wysihtml5();
      
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>