<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo e(asset('resources/views/admin/images/admin_profile/1499174950.avatar5.png')); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo e($user->name); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
			<li class="header">HOME</li>
			<li class="treeview <?php if(request()->segment(2) == 'listingCoupons' || request()->segment(2) == 'addCoupons'|| request()->segment(2) == 'editCoupons'): ?> active <?php endif; ?>">
				<a href="<?php echo e(route('admin.dashboard', ['reportBase' => 'this_month'])); ?>"> <i class="fa fa-home"></i><span> Dashboard</span></a>
			</li>
           
        </ul>
    </section>
</aside>

