<?php $__env->startSection('content'); ?>

<div class="home">
	<div class="home_background_container prlx_parent">
		<div class="home_background prlx" style="background-image:url(<?php echo asset('resources/assets/front'); ?>/images/home_background.jpg)"></div>
	</div>
	<div class="home_title">
		<h2>About us</h2>
		<div class="next_section_scroll">
			<div class="next_section nav_links" data-scroll-to=".icon_boxes">
				<i class="fas fa-chevron-down trans_200"></i>
				<i class="fas fa-chevron-down trans_200"></i>
			</div>
		</div>
	</div>
</div>
<?php echo $about->description; ?>

<div class="icon_boxes">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 icon_box_col">
				<div class="icon_box_paragraph">
					<h4>Who we are?</h4>
					<p>ALSAQR Security systems (ALSAQR) is a beacon of light for domestic and commercial market in providing Security Solutions. ALSAQR had done rigorous R & D about the product development and market study for more than 3 years and our firm officially started the operations on Feb 2012. ALSAQR Security systems carry the Trademark of “ALSAQR” which is registered with Ministry of Interior – Security system department is providing sales, installation and maintenance service to clients.</p>
				</div>
			</div>
			<div class="col-lg-6 icon_box_col">
				<div class="icon_box_paragraph">
					<h4>What we do</h4>
					<p>ALSAQR through its Security Systems Integration Division Provides Security Solutions for Home Security, Video Surveillance, Access Control, IT solution, & Vehicle tracking & Fleet Management solution.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clients">
	<div class="clients_slider_container">
		<div class="owl-carousel owl-theme clients_slider">
		  <?php if(!$brands->isEmpty()): ?>
		  <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="owl-item clients_item">
				<div class="client_item_background trans_200">
					<img src="<?php echo $brand->image; ?>" alt="">
				</div>
			</div>
		  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		  <?php endif; ?>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>