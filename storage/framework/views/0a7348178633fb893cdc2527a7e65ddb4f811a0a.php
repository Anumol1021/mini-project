<?php $__env->startSection('content'); ?>
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Edit Companies</h2>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                    <br>                       
                        <?php if(count($errors) > 0): ?>
                              <?php if($errors->any()): ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <?php echo e($errors->first()); ?>

                                </div>
                              <?php endif; ?>
                          <?php endif; ?>
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->  
						<div class="panel with-nav-tabs  panel-default">
							<!--<div class="panel-heading" style="border:none;">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#English" data-toggle="tab">English</a></li>
										<li><a href="#Arabic" data-toggle="tab">Arabic</a></li>
									</ul>
							</div>-->
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="English">
						
										 <div class="box-body">
										 
											<?php echo Form::open(array('url' =>'admin/updateCompanies', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

											  
						<?php echo Form::hidden('id',  $result['companies'][0]->id , array('class'=>'form-control', 'id'=>'id')); ?>


						<?php echo Form::hidden('oldImage',  $result['companies'][0]->logo, array('id'=>'oldImage')); ?>


						<?php echo Form::hidden('oldSmallImage',  $result['companies'][0]->small_image, array('id'=>'oldSmallImage')); ?>


						<?php echo Form::hidden('oldBannerImage',  $result['companies'][0]->banner_image, array('id'=>'oldBannerImage')); ?>

												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Title')); ?> </label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('company_name', $result['companies'][0]->company_name, array('class'=>'form-control','id'=>'company_name')); ?>

												  </div>
												</div>
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Short Description</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('short_desc', $result['companies'][0]->short_desc, array('class'=>'form-control ','id'=>'short_desc')); ?>

												  </div>
												</div>

												<div class="form-group">
					                                <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Description')); ?>  </label>
					                                <div class="col-sm-10 col-md-8">
					                                  <textarea id="editor" name="description" class="form-control" rows="10" cols="80"><?php echo $result['companies'][0]->description; ?></textarea> <br>
					                                </div>
				                             	</div>
											  
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.CompnayLogo')); ?></label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::file('newImage', array('id'=>'companies')); ?>

													<br>
												<img src="<?php echo e(asset('').$result['companies'][0]->logo); ?>" alt="" width=" 100px">
												  </div>
												</div>

													<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.CoverImage')); ?></label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::file('newSmallImage', array('id'=>'companies')); ?>

													<br>
													<img src="<?php echo e(asset('').$result['companies'][0]->small_image); ?>" alt="" width=" 100px">
												  </div>
												</div>

													<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.CompanyBannerImage')); ?></label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::file('newBannerImage', array('id'=>'companies')); ?>

													<br>
											
													<img src="<?php echo e(asset('').$result['companies'][0]->banner_image); ?>" alt="" width=" 100px">
												  </div>
												</div>

												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Link </label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('link', $result['companies'][0]->link, array('class'=>'form-control','id'=>'link')); ?>

												  </div>
												</div>


												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Status')); ?></label>
												  <div class="col-sm-10 col-md-4">
													  <select class="form-control" name="status">
														  <option value="1" <?php if($result['companies'][0]->status==1): ?> selected <?php endif; ?>><?php echo e(trans('labels.Active')); ?></option>
														  <option value="0" <?php if($result['companies'][0]->status==0): ?> selected <?php endif; ?>><?php echo e(trans('labels.Inactive')); ?></option>
													  </select>
												  </div>
												</div>

												<div class="form-group">
					                                <label for="name" class="col-sm-2 col-md-3 control-label">Product Details</label>
					                                <div class="col-sm-10 col-md-8">
					                                  <textarea id="editor" name="product_details" class="form-control" rows="10" cols="80"><?php echo $result['companies'][0]->product_details; ?></textarea> <br>
					                                </div>
				                             	</div>

												
												
											  <!-- /.box-body -->
											  <div class="box-footer text-center">
												<button type="submit" class="btn btn-primary"><?php echo e(trans('labels.Update')); ?></button>
												<a href="<?php echo e(URL::to('admin/listingCompanies')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
											  </div>
											  <!-- /.box-footer -->
											<?php echo Form::close(); ?>

										</div>
									</div>
							
						
                  </div>
              </div>
            </div>
            
          </div>
         <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
   
  </section>
  <!-- /.content --> 
  <script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>


<script type="text/javascript">
		$(function () {
			
			//bootstrap WYSIHTML5 - text editor
			//
			$("textarea").summernote({height: "400px",});
			//$("textarea").wysihtml5();
			
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>