 <div class="cta bg-blue-light">
                <div class="container">
                    <div class="row cta-1">
                        <div class="cta-features">
                            <div class="col-sm-3 blue-1"><strong>100% Satisfaction<sub>Guaranteed</sub></strong>
                            </div>
                            <div class="col-sm-3 blue-2"><strong>Free Monitoring<sub>get your healt monitored</sub></strong>
                            </div>
                            <div class="col-sm-3 blue-3"><strong>Get 15% Off<sub>Make an appointment now</sub></strong>
                            </div>
                            <div class="col-sm-3 blue-4"><strong>Call us anytime<sub>+000 123 456 7888</sub></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-4cln space-md">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-3 col-xs-6">
                                    <h6>Company</h6>
                                    <ul class="list-unstyled">
                                        <li><a href="#">About</a>
                                        </li>
                                        <li><a href="#">Meet the team</a>
                                        </li>
                                        <li><a href="#">Blog</a>
                                        </li>
                                        <li><a href="#">Contact Us</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h6>Account</h6>
                                    <ul class="list-unstyled">
                                        <li><a href="#">Payments</a>
                                        </li>
                                        <li><a href="#">Subscriptions</a>
                                        </li>
                                        <li><a href="#">Gift Card</a>
                                        </li>
                                        <li><a href="#">Support</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h6>Help</h6>
                                    <ul class="list-unstyled">
                                        <li><a href="#">Payments</a>
                                        </li>
                                        <li><a href="#">Subscriptions</a>
                                        </li>
                                        <li><a href="#">Gift Card</a>
                                        </li>
                                        <li><a href="#">Support</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <h6>Legal</h6>
                                    <ul class="list-unstyled">
                                        <li><a href="#">Payments</a>
                                        </li>
                                        <li><a href="#">Subscriptions</a>
                                        </li>
                                        <li><a href="#">Gift Card</a>
                                        </li>
                                        <li><a href="#">Support</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h6>Subscribe to our newsletter</h6>
                            <form class="form-inline" role="form">
                                <div class="input-group">
                                    <input type="email" class="form-control footernews" placeholder="Enter Email">
                                    <span class="input-group-btn">
                           <button class="btn btn-info" type="button">Subscribe</button>
                           </span>
                                </div>
                                <span class="help-block">We'll never share your address.</span>
                            </form>
                            <div class="ourTeam-social">
                                <ul class="social">
                                    <li><a href="#" class="social-btn social-facebook" title="Facebook"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li><a href="#" class="social-btn social-twitter" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li><a href="#" class="social-btn social-google" title="Google+"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li><a href="#" class="social-btn social-linkedin" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>