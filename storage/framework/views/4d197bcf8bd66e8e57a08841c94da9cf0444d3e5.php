<?php $__env->startSection('content'); ?>
<!-- Main content -->
    <section class="content">
    <?php echo $__env->make('layouts.errors-and-messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Brands</h2>
            <div class="row">
              <div class="col-xs-12">
           <div class="btn-group pull-right" >
                        <a class="btn btn-sm btn-primary" href="<?php echo e(route('admin.addBrands')); ?>"><i class="fa fa-plus"></i> Add New</a>
                 </div>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Service</th>
                      <th>Image</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php if(count($result['brands'])>0): ?>
                    <?php $__currentLoopData = $result['brands']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$brands): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($brands->id); ?></td>
                            <td style="text-transform: lowercase;"><?php echo e($brands->servicename); ?></td>
                            <td><img src="<?php echo e(asset('').'/'.$brands->image); ?>" alt="" width=" 100px"></td>
                            <td><?php echo e($brands->status); ?></td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editBrands/<?php echo e($brands->id); ?>" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                            
                            <a data-toggle="tooltip" data-placement="bottom" title="Delete" id="deletebrandsId" brandsid ="<?php echo e($brands->id); ?>" class="badge bg-red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  <?php echo e($result['brands']->links()); ?>

                </div>
              </div>
            </div>
          </div>
     </section>  

    <!-- deleteBannerModal -->
  <div class="modal fade" id="deletebrandsModal" tabindex="-1" role="dialog" aria-labelledby="deletebrandsModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="deletebrandsModalLabel">Delete </h4>
      </div>
      <?php echo Form::open(array('url' =>'admin/deleteBrands', 'name'=>'deleteBrands', 'id'=>'deleteBrands', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

          <?php echo Form::hidden('action',  'delete', array('class'=>'form-control')); ?>

          <?php echo Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'id')); ?>

      <div class="modal-body">            
        <p>Are you sure you want to delete this?</p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary" id="deleteBrands">Delete</button>
      </div>
      <?php echo Form::close(); ?>

    </div>
    </div>
  </div>
       
<?php $__env->stopSection(); ?> 
<?php $__env->startSection('js'); ?>
<script>
$(document).on('click', '#deletebrandsId', function(){
var id = $(this).attr('brandsid');
$('#id').val(id);
$("#deletebrandsModal").modal('show');
});
</script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>