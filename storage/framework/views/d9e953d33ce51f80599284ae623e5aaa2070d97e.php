<?php $__env->startSection('content'); ?>
<section class="content">
  <div class="box">
    <div class="box-body">
      <h2>Service</h2>
      <div class="box-body">
		    <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box-body">
                      <?php if( count($errors) > 0): ?>
                      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        <?php echo e($error); ?>

                      </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>
                       <?php echo Form::open(array('url' =>'admin/addNewService', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')); ?>

                      <div class="form-group">
              				  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Name')); ?></label>
              				  <div class="col-sm-10 col-md-4">
              					 <?php echo Form::text('name',  '', array('class'=>'form-control field-validate', 'id'=>'name')); ?>

              					</div>
              				</div>
                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Short description </label>
                        <div class="col-sm-10 col-md-8">
                          <textarea id="editor" name="shortdes" class="form-control" rows="10" cols="80"></textarea> <br>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Description')); ?>  </label>
                        <div class="col-sm-10 col-md-8">
                          <textarea id="editor" name="description" class="form-control" rows="10" cols="80"></textarea> <br>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Image')); ?></label>
                        <div class="col-sm-10 col-md-4">
                         <?php echo Form::file('newImage', array('id'=>'newImage')); ?>

                         <br>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Small Image</label>
                        <div class="col-sm-10 col-md-4">
                         <?php echo Form::file('shortimage', array('id'=>'shortimage')); ?>

                         <br>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Status')); ?></label>
                        <div class="col-sm-10 col-md-4">
                          <select class="form-control" name="status">
                            <option value="Active"><?php echo e(trans('labels.Active')); ?></option>
                            <option value="InActive"><?php echo e(trans('labels.InActive')); ?></option>
                          </select>
                        </div>
                      </div>
        							<div class="box-footer text-center">
        								<button type="submit" class="btn btn-primary"><?php echo e(trans('labels.SubmitNews')); ?></button>
        								<a href="<?php echo e(URL::to('admin/listingService')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
        							</div>
                       <?php echo Form::close(); ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
	</div>
</section>
<script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>
<script type="text/javascript">
		$(function () {
			$("textarea").summernote({height: "400px",});
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>