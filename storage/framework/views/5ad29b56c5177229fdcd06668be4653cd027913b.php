<?php $__env->startSection('content'); ?>
<div class="home">
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url(<?php echo asset('resources/assets/front'); ?>/images/blog_background.jpg)"></div>
		</div>
		
		<div class="home_title">
			<h2>Contact</h2>
			<div class="next_section_scroll">
				<div class="next_section nav_links" data-scroll-to=".contact">
					<i class="fas fa-chevron-down trans_200"></i>
					<i class="fas fa-chevron-down trans_200"></i>
				</div>
			</div>
		</div>
	
	</div>

	<!-- Contact -->

	<div class="contact">
		
		<div class="container">
			
			<!-- Google Map Container -->

			<div class="row">
				<div class="col">
					<div id="google_map">
						<div class="map_container">
							<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d194238.6961034716!2d51.31011209343167!3d25.22963403482553!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sqa!4v1554398757165!5m2!1sen!2sqa" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>

			<div class="row contact_row">
				<div class="col-lg-8">
					
					<!-- Reply -->

					<div class="reply">
						
						<div class="reply_title">Leave a reply</div>
						<div class="reply_form_container">
							
							<!-- Reply Form -->

							<form id="ontactForm" action="<?php echo e(route('contactus.send')); ?>" method="post">
								<?php echo e(csrf_field()); ?>

								<div>
									<input id="reply_form_name" name="name" class="input_field reply_form_name" type="text" placeholder="Name" required="required" data-error="Name is required.">
									<input id="reply_form_email" name="email" class="input_field reply_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required.">
									<input id="reply_form_subject" name="phone" class="input_field reply_form_subject" type="text" placeholder="Phone" required="required" data-error="Phone is required.">
									<textarea id="reply_form_message" name="comment" class="text_field reply_form_message"  placeholder="Message" rows="4" required data-error="Please, write us a message."></textarea>
								</div>
								<div>
									<button id="reply_form_submit" type="submit" class="reply_submit_btn trans_300" value="Submit">
										send Message
									</button>
								</div>
							</form>
						</div>
					</div>

				</div>

				<div class="col-lg-4">
					
					<!-- Contact Info -->

					<div class="contact_info">

						<div class="contact_title">Contact info</div>
						
						<div class="contact_info_container">

							<div class="logo contact_logo">
								<a href="#"><img src="<?php echo asset('resources/assets/front'); ?>/images/logo.png"></a>
							</div>
							<div class="address_container clearfix">
								<div class="contact_info_icon">i</div>
								<div class="contact_info_content">
									<ul>
										<li class="address">Building No. 79, Ali Bin Abi Taleb St. 820, Zone No. 40, Al Asiri, Doha, Qatar</li>
										<li class="phone"><strong>P:</strong> <a href="tel:+974 44583926">+974 44583926</a></li>
										<li class="city"><strong>F:</strong> +974 44621890</li>
										<li class="email"><strong>E:</strong> <a href="#">info@alsaqrsecurity.com</a></li>
									</ul>									
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
			
		
	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.appcontact', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>